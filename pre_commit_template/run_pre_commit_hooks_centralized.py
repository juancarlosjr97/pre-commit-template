"""
Module that execute the pre-commit hook centralized configuration
"""
import os


def execute_pre_commit_hooks_centralized():
    """Method that executes the pre-commit hook centralized configuration"""

    root_dir = os.path.dirname(os.path.realpath(__file__))

    pre_commit_configuration_path = os.path.join(
        root_dir, '.pre-commit-hooks-centralized.yaml')

    cmd = ['pre-commit', 'run', '--config',
           pre_commit_configuration_path, '--files']

    os.execvp(cmd[0], cmd)


if __name__ == '__main__':
    raise SystemExit(execute_pre_commit_hooks_centralized())
