#!/bin/bash

COMMIT_MESSAGE_FILE="$1"
COMMIT_MESSAGE=$(cat $COMMIT_MESSAGE_FILE)

if [[ "$COMMIT_MESSAGE" == *"\""* ]]; then
    echo "Commit message: '$COMMIT_MESSAGE'"
    echo "Your commit message cannot contain double quotes \""
    exit 1
fi
