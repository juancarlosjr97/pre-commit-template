# Contributing

When contributing to this repository, please first discuss the change you wish
to make via Slack on the `#team-datagraph-content` channel.

## Merge Request Process

Please abide by the following when submitting your change to the project.

- All changes must be submitted via a **feature branch**.
- MRs must be labelled as **draft** until ready for review. Read your MR in the
  GitLab GUI, as your reviewer will, in order to spot any last minute typos or
  unnecessarily noisy diffs.
- Feature branches must be **rebased** onto the destination branch before merge
  for a clear, linear history. Do not use merge commits.
- Commits must be squashed (**fixup**) into one commit before submitting for
  review. There may be circumstances where multiple commits are more appropriate
  but this likely means the work is too large for one MR and could instead be
  split into multiple MRs.
- Commit messages must **state the reason** for their being, rather than a
  description of implementation. In most cases this will be the name of the
  connected Jira issue e.g.
  - Bad: _Added if statement_
  - Good: _Fix item showing when modal closed_
  - ...
  - Bad: _Improved contact-form_
  - Good: _Add submit button to contact form_
- Before submitting the MR, ensure your pipeline is completing successfully.

Please contact the team on the `#team-datagraph-content` channel if you require
assistance with any of the above requirements.

## An example rebase to `fixup` commits

Let's say that you currently have the feature branch
`ABC-123 Add awesome feature` checked out, and you have the following commit
history:

```bash
> git log --oneline
```

```bash
47dbde1 (HEAD -> ABC-123 Add awesome feature, origin/ABC-123 Add awesome feature) update readme
342ab45 add test
249dfe2 wip
eb84284 Start ABC-123
9c4c8b6 (origin/main, main) ABC-122 Fix modal not opening
a616304 ABC-41 Add i18n routing
a46b620 ABC-20 Support styled components
...
```

This shows we've committed 4 times since we branched off from the `main` branch.
We'd like to turn those 4 commits into 1, at the point we branched from `main`.
First copy the commit reference of the commit we want to rebase onto, in this
case that's `9c4c8b6`. Then run the `git rebase` command and use the interactive
flag:

```bash
> git rebase -i 9c4c8b6
```

Git will open your default editor where you should see something like this:

```bash
pick eb84284 Start ABC-123
pick 249dfe2 wip
pick 342ab45 add test
pick 47dbde1 update readme
```

This is in reverse chronological order. We simply need to set the commits we'd
like to "fixup" to `f`, and `r` to reword the first commit.

```bash
r eb84284 Start ABC-123
f 249dfe2 wip
f 342ab45 add test
f 47dbde1 update readme
```

Save and close the file, and git will begin the rebase. It'll stop when it gets
to your reword instruction where you can change the text of the commit message.
Again, save and close the file, and git will finish the rebase. Check that the
history has been updated:

```bash
> git log --oneline
```

```bash
234892a (HEAD -> ABC-123 Add awesome feature) ABC-123 Add awesome feature
9c4c8b6 (origin/main, main) ABC-122 Fix modal not opening
a616304 ABC-41 Add i18n routing
a46b620 ABC-20 Support styled components
...
```

Now, you'll notice the commit reference for the one commit we consolidated into
has changed from the original reference. This is because we've rewritten
history. Whilst a powerful tool, this can be quite dangerous, so make sure to
only do this on _your_ feature branches. We now need to tell origin (GitLab) of
your changes. **Do not pull!** This will overwrite your rebase changes. You need
to _force_ push your changes over the top of the remote history:

```bash
> git push -f
```

Quick tip: if you'd like to use VSCode as your interactive rebase editor, run
the following command.

```bash
> git config --global core.editor "code --wait"
```
