# Code Standards

This repository contains the centralized code standards configuration for the Datagraph & Content Team.

## Prerequisites

- [GIT](https://git-scm.com/)
- Python `v3.9.5`
- [Poetry](https://python-poetry.org/docs/#installation) `v1.1.7+`
- [pyenv](https://github.com/pyenv/pyenv)
- [RS JFrog](https://rs-components.atlassian.net/wiki/spaces/BER/pages/3306881139/Using+JFrog+as+an+npm+registry)
- [pre-commit](https://pre-commit.com/#install)
- [tflint](https://github.com/terraform-linters/tflint)
- [terraform](https://rs-components.atlassian.net/wiki/spaces/TH/pages/1085014045/On-Boarding+for+New+Developers#On-BoardingforNewDevelopers-SettingupTerraform/Terragrunt)

## Usage

## Development

Before start a contribution, please read the [CONTRIBUTING.md](CONTRIBUTING.md) document.

### Python

Check you are using the correct python version by running

```bash
pyenv local
```

### Poetry

To install dependencies run in the terminal

```bash
poetry install
```

## Delete Poetry Virtual Environment

```bash
poetry env remove python
```
